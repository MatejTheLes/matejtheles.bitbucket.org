
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';
var user=1;
var timeTable = new Array();
var weightTable = new Array();



//IMPLEMENTACIJA DRUGEGA API, ZNANI LJUDJE
var api_key = 'c4e277bde6eb6e6afa8ebf7c5b5c3de6';

var resource_url = 'https://api.betterdoctor.com/2016-03-01/doctors?location=37.773,-122.413,100&skip=0&limit=10&user_key=' + api_key;



$.get(resource_url, function (data) {
    var template = Handlebars.compile(document.getElementById('docs-template').innerHTML);
    document.getElementById('content-placeholder').innerHTML = template(data);
});
//KONEC IMPLEMENTACIJE DRUGEGA API

var nameX;
var surnameX;
var birthdateX;


var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */

 function generirajPodatke(stPacienta){
	sessionId = getSessionId();
	var ehrId;
	if(stPacienta == 1){
	var ime ="Matej";
	var priimek = "Leskovšek";
	var datumRojstva = "1997-03-08T02:02";
	}else if(stPacienta == 2){
	    var ime = "Dejan";
    	var priimek = "Lavbic";
	    var datumRojstva = "1980-04-04T08:50";
	}else if(stPacienta == 3){
	    var ime = "Kronično Bolan";
    	var priimek = "Študent";
	    var datumRojstva = "1997-01-01T01:01";    
	}

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId;
				
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
							$(function(){
								if(user<=3){
									document.getElementById(user).innerHTML = ime + " " + priimek;
									document.getElementById(user).setAttribute("value", ehrId);
									if(user == 1){
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-05-27T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "89.2");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "98");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 100);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-05-30T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "89");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "98");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 200);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-10T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "87.5");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "99");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 300);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-20T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "86.7");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "99");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 400);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-30T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "85");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "99");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 500);
										document.getElementById("something").innerHTML += ehrId + "</br>";
										document.getElementById("pacienti").innerHTML += ehrId + "</br>";
									}
									if(user == 2){
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-05-27T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "86");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "98");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 100);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-05-30T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "86.2");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "98");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 200);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-10T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "86.5");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "99");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 300);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-20T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "86.1");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "99");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 400);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-30T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "85.7");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "99");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 500);
										document.getElementById("something").innerHTML += ehrId + "</br>";
										document.getElementById("pacienti").innerHTML += ehrId + "</br>";
									}
									if(user == 3){
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-05-27T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "89.2");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "98");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 100);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-05-30T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "89.6");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "97");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 200);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-10T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "93.5");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "96");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 300);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-20T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "96");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "95");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 400);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-30T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "100.1");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "94");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 500);
										document.getElementById("something").innerHTML += ehrId + "</br>";
										document.getElementById("pacienti").innerHTML += ehrId + "</br>";
									}
									user++;
								}else{
									user = 1;
									document.getElementById(user).innerHTML = ime + " " + priimek;
									document.getElementById(user).setAttribute("value", ehrId);
									if(user == 1){
									    document.getElementById("something").innerHTML += ehrId + "</br>";
										document.getElementById("pacienti").innerHTML += ehrId + "</br>";
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-05-27T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "89.2");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "98");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 100);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-05-30T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "89");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "98");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 200);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-10T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "87.5");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "99");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 300);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-20T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "86.7");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "99");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 400);
										setTimeout(function(){
											document.getElementById("dodajVitalnoEHR").setAttribute("value", ehrId);
											document.getElementById("dodajVitalnoDatumInUra").setAttribute("value", "2017-06-30T09:01");
											document.getElementById("dodajVitalnoTelesnaVisina").setAttribute("value", "178");
											document.getElementById("dodajVitalnoTelesnaTeza").setAttribute("value", "85");
											document.getElementById("dodajVitalnoTelesnaTemperatura").setAttribute("value", "36.5");
											document.getElementById("dodajVitalnoKrvniTlakSistolicni").setAttribute("value", "120");
											document.getElementById("dodajVitalnoKrvniTlakDiastolicni").setAttribute("value", "80");
											document.getElementById("dodajVitalnoNasicenostKrviSKisikom").setAttribute("value", "99");
											document.getElementById("dodajVitalnoMerilec").setAttribute("value", "medicinski brat študent Matej");
											setTimeout(function(){dodajMeritveVitalnihZnakov()});
										}, 500);
									}
									user++;
								}
							})
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
	
	return ehrId;
 }
 
function kreirajEHRzaBolnika() {
	document.getElementById("something").innerHTML = " ";
	document.getElementById("rezultatMeritveVitalnihZnakov").innerHTML = " ";
	setTimeout(function(){
		nameX = "Matej";
		surnameX = "Leskovšek";
		birthdateX="1997-03-08T02:02";
		var ehrId1 = generirajPodatke(1);
	}, 0);
	
	setTimeout(function(){
		nameX = "Dejan";
		surnameX = "Lavbič";
		birthdateX="1980-04-04T08:50";
		var ehrId2 = generirajPodatke(2);
	}, 1000);
	
	
	setTimeout(function(){
		nameX = "Kronično Bolan";
		surnameX = "Študent";
		birthdateX="1997-01-01T01:01";
		var ehrId3 = generirajPodatke(3);
	}, 2000);
	
	setTimeout(function(){
		document.getElementById('pacienti').innerHTML += "Podatki o pacientih uspešno posodobljeni!</br>";
	}, 3000);
	
}




//$('document').ready(kreirajEHRzaBolnika());


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
	var merilec = $("#dodajVitalnoMerilec").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura, filtriranje telesne temperature in telesna teža).
 * Filtriranje telesne temperature je izvedena z AQL poizvedbo, ki se uporablja
 * za napredno iskanje po zdravstvenih podatkih.
 */
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span> Podatki " +
          "o <b>'" + tip + "'</b> za pacienta <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "telesna temperatura") {
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].temperature +
                          " " + res[i].unit + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#pacienti").html(
                  "<span class='obvestilo label label-danger fade-in' style='width:150px;'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!</br>");
			$("#something").html(
            "<span class='obvestilo label label-danger fade-in' style='width:150px;'>Napaka! Preverite log!");
					    }
					});
				} else if (tip == "telesna teža") {
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td>";
						  
									timeTable[i] = res[i].time;
									weightTable[i] = res[i].weight;
									document.getElementsByClassName(".chart").innerHTML = " ";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
								/*for(var i in timeTable){
									document.getElementById('pacienti').innerHTML += timeTable[i]+"</br>";
								}
								for(var i in timeTable){
									document.getElementById('pacienti').innerHTML += weightTable[i]+"</br>";
								}*/
								for(var i in timeTable){
									data[i] = {time: timeTable[i], weight: weightTable[i]};
								}
								
								
								
								document.getElementById("something").innerHTML = " ";
								createGraph(data);
								
								
								
								
								document.getElementById('pacienti').innerHTML += "Podatki o teži pacienta '" + party.firstNames + " " + party.lastNames + "' pridobljeni.</br>";
								
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#pacienti").html(
                  "<span class='obvestilo label label-danger fade-in' style='width:150px;'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!</br>");
			$("#something").html(
            "<span class='obvestilo label label-danger fade-in' style='width:150px;'>Napaka! Preverite log!");
					    }
					});
				} else if (tip == "telesna temperatura AQL") {
					var AQL =
						"select " +
    						"t/data[at0002]/events[at0003]/time/value as cas, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude as temperatura_vrednost, " +
    						"t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/units as temperatura_enota " +
						"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
						"contains OBSERVATION t[openEHR-EHR-OBSERVATION.body_temperature.v1] " +
						"where t/data[at0002]/events[at0003]/data[at0001]/items[at0004]/value/magnitude<35 " +
						"order by t/data[at0002]/events[at0003]/time/value desc " +
						"limit 10";
					$.ajax({
					    url: baseUrl + "/query?" + $.param({"aql": AQL}),
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var results = "<table class='table table-striped table-hover'>" +
                  "<tr><th>Datum in ura</th><th class='text-right'>" +
                  "Telesna temperatura</th></tr>";
					    	if (res) {
					    		var rows = res.resultSet;
						        for (var i in rows) {
						            results += "<tr><td>" + rows[i].cas +
                          "</td><td class='text-right'>" +
                          rows[i].temperatura_vrednost + " " 	+
                          rows[i].temperatura_enota + "</td>";
						        }
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}

					    },
					    error: function() {
					    	$("#pacienti").html( 
                  "<span class='obvestilo label label-danger fade-in' style='width:150px;'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!</br>");
			$("#something").html(
            "<span class='obvestilo label label-danger fade-in' style='width:150px;'>Napaka! Preverite log!");
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		$("#pacienti").html(
            "<span class='obvestilo label label-danger fade-in' style='width:150px;'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!</br>");
			$("#something").html(
            "<span class='obvestilo label label-danger fade-in' style='width:150px;'>Napaka! Preverite log!");
	    	}
		});
	}
}


function createGraph(){
	var data = arguments[0];
	var lengthX = timeTable.length;
	var temp;
	for(var i = 0; i <= (lengthX/2); i++){
		temp = weightTable[i];
		weightTable[i]= weightTable[lengthX-1-i];
		weightTable[lengthX-1-i] = temp;
	}
	for(var i = 0; i <= (lengthX/2); i++){
		temp = timeTable[i];
		timeTable[i]= timeTable[lengthX-1-i];
		timeTable[lengthX-1-i] = temp;
	}
	
	
	
	var data = weightTable;
	var margins = 20;
	var x = d3.scaleLinear()
		.domain([0, d3.max(data)])
		.range([0, 170]);
	
	d3.select(".chart")
	  .selectAll("div")
		.data(data)
	  .enter().append("div")
		.style("width", function(d) { return x(d) + "px"; })
		.style("height", "30px")
		.style("margin-top", "-2px")
		.style("border-style", "solid")
		.style("border-color", "black")
		.style('background-color', function (d) { return d.backgroundColor; })
		// add mouseover effect to change background color to black
		.on('mouseover', function() {
			d3.select(this)
			.style('background-color','black')
			.text(function(d) { 
			var vrni = "";
			for(var i = 0; i < weightTable.length; i++){
				if(d == weightTable[i]){
					vrni = timeTable[i];
				}
			}
			
			return vrni.substring(0, 10); });
		})
		.on('mouseout', function () {
			d3.select(this)
			.style('background-color', function (d) { return d.backgroundColor; })
			.text(function(d) { return d + " kg"; });
		})
		//.style("transform", "rotate(-90deg)")
		//.style("margin-top", "20px")
		.text(function(d) { return d + " kg"; });
		

}



$(document).ready(function() {
	document.getElementById('pacienti').innerHTML += "Stran naložena.</br>";
  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());

		document.getElementById("something").innerHTML = " ";
		document.getElementById("rezultatMeritveVitalnihZnakov").innerHTML = " ";
		preberiMeritveVitalnihZnakov();
	});

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});